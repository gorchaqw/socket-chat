package main

import (
	"flag"

	"socket-chat/modules/chat/usecase"

	wsChat "socket-chat/modules/chat/delivery/ws"
	chatRepo "socket-chat/modules/chat/repository/localstorage"

	"log"

	"net/http"
)

var addr = flag.String("addr", ":8080", "http service address")

type App struct {
	httpServer *http.Server
	chatUC     *usecase.ChatUseCase
}

func NewApp() *App {
	userRepo := chatRepo.NewUserRepository()
	hubRepo := chatRepo.NewHubRepository()
	return &App{
		chatUC: usecase.NewChatUseCase(&userRepo, &hubRepo),
	}
}
func (a *App) Run() {
	wsChat.RegisterEndpoints(a.chatUC)

	err := http.ListenAndServe(*addr, nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
func main() {
	app := NewApp()
	app.Run()

}
