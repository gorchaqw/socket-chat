run 
``` 
go mod tidy
go run ./ 
```

auth
```
http://localhost:8080/auth?user=user1&pass=pass1
```

logout
```
http://localhost:8080/logout?token=9c5d1f49-9097-48b4-9713-0c2de3839cc8
```

view in browser
``` 
http://localhost:8080/?chatId=1&token=2d8d6c0d-59f6-461a-a82e-b870d48a21bb
http://localhost:8080/?chatId=2&token=2d8d6c0d-59f6-461a-a82e-b870d48a21bb
...
http://localhost:8080/?chatId=N&token=Z
```

source
```
https://github.com/gorilla/websocket/tree/master/examples/chat
```

users json 
```
{
  "users": [
    {
      "login": "user1",
      "password": "pass1"
    },
    {
      "login": "user2",
        "password": "pass2"
    }
  ]
}
```