package ws

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"github.com/gorilla/websocket"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"socket-chat/modules/chat/models"
	"socket-chat/modules/chat/usecase"
	"strconv"
	"time"
)

const (
	writeWait      = 10 * time.Second
	pongWait       = 60 * time.Second
	pingPeriod     = (pongWait * 9) / 10
	maxMessageSize = 512
)

var (
	newline = []byte{'\n'}
	space   = []byte{' '}
)

type Handler struct {
	useCase  *usecase.ChatUseCase
	upgrader *websocket.Upgrader
	userList models.Users
}

func NewHandler(uC *usecase.ChatUseCase, upgrader *websocket.Upgrader) Handler {
	jsonFile, _ := os.Open("users.json")
	defer func() {
		_ = jsonFile.Close()
	}()

	byteValue, _ := ioutil.ReadAll(jsonFile)

	var users models.Users
	_ = json.Unmarshal(byteValue, &users)

	return Handler{
		useCase:  uC,
		upgrader: upgrader,
		userList: users,
	}
}

func (h *Handler) serveAuth(w http.ResponseWriter, r *http.Request) {
	user := r.URL.Query().Get("user")
	pass := r.URL.Query().Get("pass")
	var token string

	if user != "" && pass != "" {
		for _, u := range h.userList.Users {
			if u.Login == user && u.Pass == pass {
				token = uuid.New().String()
				h.useCase.Login(token, u)
			}
		}
	}

	if token == "" {
		http.Error(w, "Forbidden", http.StatusForbidden)
	}

	if _, err := w.Write([]byte(fmt.Sprintf(`<a href="http://localhost:8080/?chatId=1&token=%s">http://localhost:8080/?chatId=1&token=%s</a>`, token, token))); err != nil {
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
	}

}
func (h *Handler) serveHome(w http.ResponseWriter, r *http.Request) {

	if r.URL.Path != "/" {
		http.Error(w, "Not found", http.StatusNotFound)
		return
	}
	if r.Method != "GET" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	http.ServeFile(w, r, "home.html")
}
func (h *Handler) serveLogout(w http.ResponseWriter, r *http.Request) {
	token := r.URL.Query().Get("token")
	if token != "" {
		h.useCase.Logout(token)
	}
	if _, err := w.Write([]byte("ok")); err != nil {
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
	}
}
func (h *Handler) serveWebsocket(w http.ResponseWriter, r *http.Request) {
	chatIdVal := r.URL.Query().Get("chatId")
	token := r.URL.Query().Get("token")

	if chatIdVal != "" && token != "" {
		i, err := strconv.Atoi(chatIdVal)
		if err != nil {
			http.Error(w, "Bad Request", http.StatusBadRequest)
			return
		}

		user := h.useCase.GetUserByToken(token)
		if user == nil {
			http.Error(w, "Forbidden", http.StatusForbidden)
		}

		if h.useCase.GetHubById(i) == nil {
			h.useCase.IncHub(i, models.NewHub())
		}

		h.serveWs(h.useCase.GetHubById(i), w, r)
	}
}
func (h *Handler) serveWs(hub *models.Hub, w http.ResponseWriter, r *http.Request) {
	h.upgrader.CheckOrigin = func(r *http.Request) bool { return true }

	conn, err := h.upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}

	client := models.NewClient(hub, conn)
	client.Hub.Register <- client

	go h.writePump(client)
	go h.readPump(client)
}
func (h *Handler) readPump(c *models.Client) {
	defer func() {
		c.Hub.Unregister <- c
		c.Conn.Close()
	}()
	c.Conn.SetReadLimit(maxMessageSize)
	c.Conn.SetReadDeadline(time.Now().Add(pongWait))
	c.Conn.SetPongHandler(func(string) error { c.Conn.SetReadDeadline(time.Now().Add(pongWait)); return nil })
	for {
		_, message, err := c.Conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Printf("error: %v", err)
			}
			break
		}

		message = bytes.TrimSpace(bytes.Replace(message, newline, space, -1))
		c.Hub.Broadcast <- message
	}
}
func (h *Handler) writePump(c *models.Client) {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		c.Conn.Close()
	}()
	for {
		select {
		case message, ok := <-c.Send:
			c.Conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				c.Conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			w, err := c.Conn.NextWriter(websocket.TextMessage)
			if err != nil {
				return
			}
			w.Write(message)

			n := len(c.Send)
			for i := 0; i < n; i++ {
				w.Write(newline)
				w.Write(<-c.Send)
			}

			if err := w.Close(); err != nil {
				return
			}
		case <-ticker.C:
			c.Conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.Conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
	}
}
