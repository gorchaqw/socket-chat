package ws

import (
	"github.com/gorilla/websocket"
	"net/http"
	"socket-chat/modules/chat/usecase"
)

func RegisterEndpoints(uc *usecase.ChatUseCase) {
	h := NewHandler(uc, &websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
	})

	http.HandleFunc("/auth", h.serveAuth)
	http.HandleFunc("/", h.serveHome)
	http.HandleFunc("/logout", h.serveLogout)
	http.HandleFunc("/ws", h.serveWebsocket)
}
