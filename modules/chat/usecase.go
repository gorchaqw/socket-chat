package chat

import "socket-chat/modules/chat/models"

type UseCase interface {
	Login(token string, u *models.User)
	Logout(token string)
	GetUserByToken(token string) *models.User
	GetHubById(i int) *models.Hub
	IncHub(i int, hub *models.Hub)
}
