package localstorage

import (
	"socket-chat/modules/chat/models"
	"sync"
)

type UserRepository struct {
	Store map[string]*models.User
	mux   sync.Mutex
}

func NewUserRepository() UserRepository {
	return UserRepository{
		Store: make(map[string]*models.User),
	}
}

func (u *UserRepository) Inc(token string, user *models.User) {
	u.mux.Lock()
	u.Store[token] = user
	u.mux.Unlock()
}

func (u *UserRepository) Value(token string) *models.User {
	u.mux.Lock()
	defer u.mux.Unlock()

	return u.Store[token]
}

func (u *UserRepository) Drop(token string) {
	u.mux.Lock()
	defer u.mux.Unlock()

	delete(u.Store, token)
}
