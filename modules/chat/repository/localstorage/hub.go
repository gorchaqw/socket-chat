package localstorage

import (
	"socket-chat/modules/chat/models"
	"sync"
)

type HubRepository struct {
	Store map[int]*models.Hub
	mux   sync.Mutex
}

func NewHubRepository() HubRepository {
	return HubRepository{
		Store: make(map[int]*models.Hub),
	}
}

func (h *HubRepository) Inc(id int, hub *models.Hub) {
	h.mux.Lock()
	h.Store[id] = hub
	go h.Store[id].Run()
	h.mux.Unlock()
}

func (h *HubRepository) Value(id int) *models.Hub {
	h.mux.Lock()
	defer h.mux.Unlock()

	return h.Store[id]
}
