package models

import "github.com/gorilla/websocket"

type Client struct {
	Hub  *Hub
	Conn *websocket.Conn
	Send chan []byte
}

func NewClient(hub *Hub, conn *websocket.Conn) *Client {
	return &Client{Hub: hub, Conn: conn, Send: make(chan []byte, 256)}
}
