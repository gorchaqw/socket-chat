package models

type Users struct {
	Users []*User `json:"users"`
}

type User struct {
	Login string `json:"login"`
	Pass  string `json:"password"`
}
