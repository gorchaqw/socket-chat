package usecase

import (
	"socket-chat/modules/chat"
	"socket-chat/modules/chat/models"
)

type ChatUseCase struct {
	userRepo chat.UserRepository
	hubRepo  chat.HubRepository
}

func NewChatUseCase(uR chat.UserRepository, hR chat.HubRepository) *ChatUseCase {
	return &ChatUseCase{
		userRepo: uR,
		hubRepo:  hR,
	}
}
func (uC *ChatUseCase) Login(token string, u *models.User) {
	uC.userRepo.Inc(token, u)
}
func (uC *ChatUseCase) Logout(token string) {
	uC.userRepo.Drop(token)
}
func (uC *ChatUseCase) GetUserByToken(token string) *models.User {
	return uC.userRepo.Value(token)
}

func (uC *ChatUseCase) GetHubById(i int) *models.Hub {
	return uC.hubRepo.Value(i)
}
func (uC *ChatUseCase) IncHub(i int, hub *models.Hub) {
	uC.hubRepo.Inc(i, hub)
}
