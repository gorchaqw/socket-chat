package chat

import "socket-chat/modules/chat/models"

type UserRepository interface {
	Inc(token string, user *models.User)
	Value(token string) *models.User
	Drop(token string)
}

type HubRepository interface {
	Inc(id int, hub *models.Hub)
	Value(id int) *models.Hub
}
